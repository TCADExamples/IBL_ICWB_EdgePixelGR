## Back side processing
#setdep @node|-1:all@

init tdr=n@previous@

sde on

transform flip

icwb filename=TCAD_IBL.tcl scale=0.001
icwb domain=@Domain@

AdvancedCalibration 2016.12

math numThreads=16
math numThreadsSnMesh = 16
grid remesh
pdbSet Grid Adaptive 1
pdbSet Grid AdaptiveField Refine.Abs.Error 1e25
pdbSet Grid AdaptiveField Refine.Rel.Error 1e25
pdbSet Grid Phosphorus Refine.Min.Value 1e25
pdbSet Grid Phosphorus Refine.Max.Value 1e25
#pdbSet Grid Phosphorus Refine.Target.Length 100
pdbSet Grid Boron Refine.Min.Value 1e25
pdbSet Grid Boron Refine.Max.Value 1e25
#pdbSet Grid Boron Refine.Target.Length 100

deposit material = {Oxide} type = isotropic  rate = {@THICKOX_THICKNESS@} time=1

icwb.create.mask layer.name= BSIMPLANT name= BSIMPLANT polarity= positive 
#photo mask=BSIMPLANT thickness=2
deposit thickness= 2 Resist isotropic mask = BSIMPLANT 


#set etch_depth @< @THICKOX_THICKNESS@ - @SCREENOX_THICKNESS@ >@

etch material = {Oxide} type=anisotropic rate = {@etch_depth@} time=1 

pdbSet Grid AdaptiveField Refine.Rel.Error 1.2
pdbSet Grid SnMesh min.normal.size 0.1

implant Boron dose=@PDOSE@ energy=@PENERGY@ tilt=7 
strip Resist 

icwb.create.mask layer.name= BSCONTACT name=BSCONTACT polarity= positive
photo mask=BSCONTACT  thickness=2
etch material = {Oxide} type=anisotropic rate = {10.0} time=1 
strip Resist 


struct tdr=n@node@





