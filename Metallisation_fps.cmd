#setdep @previous@

icwb filename=TCAD_IBL.tcl scale=0.001
icwb domain=@Domain@

AdvancedCalibration 2016.12

sde on

init tdr=n@previous@

## ------------------------------------
## ---- Added process flow header -----
## ------------------------------------
math numThreads=16
math numThreadsSnMesh = 16
math numThreadsMGoals = 16
grid remesh
pdbSet Grid Adaptive 1
pdbSet Grid AdaptiveField Refine.Abs.Error 1e25
pdbSet Grid AdaptiveField Refine.Rel.Error 1e25
pdbSet Grid Phosphorus Refine.Min.Value 1e25
pdbSet Grid Phosphorus Refine.Max.Value 1e25
#pdbSet Grid Phosphorus Refine.Target.Length 100
pdbSet Grid Boron Refine.Min.Value 1e25
pdbSet Grid Boron Refine.Max.Value 1e25
#pdbSet Grid Boron Refine.Target.Length 100

pdbSet Grid AdaptiveField Refine.Rel.Error 1.0
pdbSet Grid SnMesh min.normal.size 0.1


deposit material = {Aluminum} type = isotropic  rate = {1.0} time=1.5
icwb.create.mask layer.name= BSMETAL name= BSMETAL polarity= negative
photo mask=BSMETAL thickness=2
etch material = {Aluminum} type=anisotropic rate = {10.0} time=1 
strip Resist 


transform flip

deposit material = {Aluminum} type = isotropic  rate = {1.0} time=1.0
icwb.create.mask layer.name= PIXEL name= PIXEL polarity= negative
photo mask=PIXEL thickness=2
etch material = {Aluminum} type=anisotropic rate = {10.0} time=1 suppress.remesh
strip Resist 
struct tdr=n@node@

exit

