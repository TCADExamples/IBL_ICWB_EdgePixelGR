#setdep @node|-1:all@

icwb filename=TCAD_IBL.tcl scale=0.001
icwb domain=@Domain@

AdvancedCalibration 2016.12


init tdr=n@previous@

## ------------------------------------
## ---- Added process flow header -----
## ------------------------------------
math numThreads=16
math numThreadsSnMesh = 16
math numThreadsMGoals = 16
grid remesh
pdbSet Grid Adaptive 1
pdbSet Grid AdaptiveField Refine.Abs.Error 1e25
pdbSet Grid AdaptiveField Refine.Rel.Error 1e25
pdbSet Grid Phosphorus Refine.Min.Value 1e25
pdbSet Grid Phosphorus Refine.Max.Value 1e25
#pdbSet Grid Phosphorus Refine.Target.Length 100
pdbSet Grid Boron Refine.Min.Value 1e25
pdbSet Grid Boron Refine.Max.Value 1e25
#pdbSet Grid Boron Refine.Target.Length 100

pdbSet Grid AdaptiveField Refine.Rel.Error 1.0
pdbSet Grid SnMesh min.normal.size 0.1

temp_ramp name=tempramp_@node@ time=240 temp=950 
diffuse temp_ramp=tempramp_@node@ 

struct tdr=n@node@

exit
