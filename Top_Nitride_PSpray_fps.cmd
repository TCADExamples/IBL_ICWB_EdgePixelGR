
icwb filename=TCAD_IBL.tcl scale=0.001
icwb domain=@Domain@

AdvancedCalibration 2016.12

sde on 

set Ymin [icwb bbox left] ; set Ymax [icwb bbox right] 
set Zmin [icwb bbox back] ; set Zmax [icwb bbox front]

set DIM [icwb dimension]

#set half @< @thickness@/2 >@

line x loc=0 spa=0.01<um> tag=top
line x loc=1 spa=0.03<um> 
line x loc=@half@<um> spa=50<um> 
line x loc=@<@thickness@-1>@ spa=0.03<um> 
line x loc=@thickness@ spa=0.01<um> tag = bot

if { $DIM == 3 } { 		line y loc= $Ymin spa=20<um> tag=left
		line y loc=$Ymax spa=20<um> tag=right
		
		line z loc=$Zmin spa=20.0<um> tag=front
		line z loc=$Zmax spa=20.0<um> tag=back
		set Ydim "ylo=left yhi=right"; set Zdim "zlo=front zhi=back" } 
if { $DIM == 2 } { 
 		line y loc= $Ymin spa=20<um> tag=left
		line y loc=$Ymax<um> spa=20<um> tag=right
		set Ydim "ylo=left yhi=right" ; set Zdim "" } 


eval region silicon xlo=top xhi=bot $Ydim $Zdim
init concentration=8.99e+11 field=Phosphorus wafer.orient=100 

## ------------------------------------
## ---- Added process flow header -----
## ------------------------------------
math numThreads=16
math numThreadsSnMesh = 16
math numThreadsMGoals = 16
math maxNumberOfDomains=24

grid remesh

pdbSet Grid Adaptive 1
pdbSet Grid AdaptiveField Refine.Abs.Error 1e25
pdbSet Grid AdaptiveField Refine.Rel.Error 1e25
pdbSet Grid Phosphorus Refine.Min.Value 1e25
pdbSet Grid Phosphorus Refine.Max.Value 1e25
#pdbSet Grid Phosphorus Refine.Target.Length 100
pdbSet Grid Boron Refine.Min.Value 1e25
pdbSet Grid Boron Refine.Max.Value 1e25
#pdbSet Grid Boron Refine.Target.Length 100

pdbSet Grid AdaptiveField Refine.Rel.Error 1.0
pdbSet Grid SnMesh min.normal.size 0.1

deposit material = {Oxide} type = isotropic  rate = {@THICKOX_THICKNESS@} time=1
deposit material = {Nitride} type = anisotropic  rate = {@NITRIDE_THICKNESS@} time=1

icwb.create.mask layer.name= PSPRAY name= PSPRAY polarity= positive
photo mask=PSPRAY thickness=2

refinebox name= pspray_edges mask= PSPRAY extend= 1 extrusion.min= -1.0 extrusion.max= 1.0  xrefine= 0.2 yrefine= 0.2

etch material = {Nitride} type=anisotropic rate = {@NITRIDE_THICKNESS@} time=1 suppress.remesh
strip Resist 

implant Boron dose=@PSPRAY_DOSE@ energy=@PSPRAY_ENERGY@ tilt=0 rot=-90 

struct tdr=n@node@

exit


