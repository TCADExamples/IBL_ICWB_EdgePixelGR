#setdep @node|-1:all@

icwb filename=TCAD_IBL.tcl scale=0.001
icwb domain=@Domain@

AdvancedCalibration 2016.12

sde on

init tdr=n@previous@

## ------------------------------------
## ---- Added process flow header -----
## ------------------------------------
math numThreads=16
math numThreadsSnMesh = 16
math numThreadsMGoals = 16
grid remesh
pdbSet Grid Adaptive 1
pdbSet Grid AdaptiveField Refine.Abs.Error 1e25
pdbSet Grid AdaptiveField Refine.Rel.Error 1e25
pdbSet Grid Phosphorus Refine.Min.Value 1e25
pdbSet Grid Phosphorus Refine.Max.Value 1e25
#pdbSet Grid Phosphorus Refine.Target.Length 100
pdbSet Grid Boron Refine.Min.Value 1e25
pdbSet Grid Boron Refine.Max.Value 1e25
#pdbSet Grid Boron Refine.Target.Length 100

pdbSet Grid AdaptiveField Refine.Rel.Error 1.0
pdbSet Grid SnMesh min.normal.size 0.1


icwb.create.mask layer.name= NPLUS name= NPLUS polarity= positive 
deposit thickness= 2 Resist isotropic mask = NPLUS 
#photo mask=NPLUS thickness=2

refinebox name= nplus_edges mask= NPLUS extend= 1 extrusion.min= -1.0 extrusion.max= 1.0  xrefine= 0.2 yrefine= 0.2

#set etch_depth @< @THICKOX_THICKNESS@ - @SCREENOX_THICKNESS@ >@
etch material = {Nitride} type=anisotropic rate = {@NITRIDE_THICKNESS@} time=1 suppress.remesh
etch material = {Oxide} type=anisotropic rate = {@etch_depth@} time=1 suppress.remesh



implant Phosphorus dose=@NDOSE@ energy=@NENERGY@ tilt=0 rot=-90 
strip Resist 

struct tdr=after_oxide_etch_n@node@


icwb.create.mask layer.name= OXIDE_HOLES name= OXIDE_HOLES polarity= positive
photo mask=OXIDE_HOLES  thickness=2
etch material = {Oxide} type=anisotropic rate = {0.5} time=1 suppress.remesh
strip Resist 
struct tdr=n@node@

exit


